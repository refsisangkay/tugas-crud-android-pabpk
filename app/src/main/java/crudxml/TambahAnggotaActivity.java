package crudxml;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dharma.crudxml.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public class TambahAnggotaActivity extends Activity {

	// Progress Dialog
    private ProgressDialog pDialog;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TextView tvDateResult;
    private Button btDatePicker;

    EditText inputNama;
    EditText inputAlamat;
    RadioGroup radioGroup;
    CheckBox id,en,kr,jp;

    String jk,bahasa="",tanggal,agama;
    private Spinner spinner1;


    // upload image

    Bitmap bitmap;

    boolean check = true;

    Button SelectImageGallery, UploadImageServer;

    ImageView imageView;

    EditText imageName;

    ProgressDialog progressDialog ;

    String GetImageNameEditText;

    String ImageName = "image_name" ;

    String ImagePath = "image_path" ;

    String ServerUploadPath ="http://178.128.83.56/crudxmlphp/img_upload_to_server.php";

    //--

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tambah_anggota);

        // definisikan Edit Text
        inputNama = (EditText) findViewById(R.id.inputNama);
        inputAlamat = (EditText) findViewById(R.id.inputAlamat);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        id = (CheckBox) findViewById(R.id.checkBox);
        en = (CheckBox) findViewById(R.id.checkBox2);
        kr = (CheckBox) findViewById(R.id.checkBox3);
        jp = (CheckBox) findViewById(R.id.checkBox4);


/**
 * Kita menggunakan format tanggal dd-MM-yyyy
 * jadi nanti tanggal nya akan diformat menjadi
 * misalnya 01-12-2017
 */
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        tvDateResult = (TextView) findViewById(R.id.tv_dateresult);
        btDatePicker = (Button) findViewById(R.id.bt_datepicker);
        btDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb=(RadioButton)findViewById(checkedId);
                //setText("Kamu Memilih" + rb.getText());
                jk = (String) rb.getText();
                Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();

            }
        });

        spinner1 = (Spinner) findViewById(R.id.spinner);
        spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());





        // definisikan button
        Button btnTambahAnggota = (Button) findViewById(R.id.btnTambahAnggota);

        // button click event untuk simpan penambahan data
        btnTambahAnggota.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // buat method BUatAnggotaBaru untuk background thread
                new BuatAnggotaBaru().execute();
            }
        });

        imageView = (ImageView)findViewById(R.id.imageView);

        imageName = (EditText)findViewById(R.id.editTextImageName);

        SelectImageGallery = (Button)findViewById(R.id.buttonSelect);

        UploadImageServer = (Button)findViewById(R.id.buttonUpload);

        SelectImageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();

                intent.setType("image/*");

                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), 1);

            }
        });


        UploadImageServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GetImageNameEditText = imageName.getText().toString();

                ImageUploadToServerFunction();

            }
        });
    }

    @Override
    protected void onActivityResult(int RC, int RQC, Intent I) {

        super.onActivityResult(RC, RQC, I);

        if (RC == 1 && RQC == RESULT_OK && I != null && I.getData() != null) {

            Uri uri = I.getData();

            try {

                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                imageView.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

    public void ImageUploadToServerFunction(){

        ByteArrayOutputStream byteArrayOutputStreamObject ;

        byteArrayOutputStreamObject = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);

        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();

        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

        class AsyncTaskUploadClass extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                progressDialog = ProgressDialog.show(TambahAnggotaActivity.this,"Menambah Data Angggota","Please Wait",false,false);
            }

            @Override
            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                // Dismiss the progress dialog after done uploading.
                progressDialog.dismiss();

                // Printing uploading success message coming from server on android app.
                Toast.makeText(TambahAnggotaActivity.this,string1,Toast.LENGTH_LONG).show();

                // Setting image as transparent after done uploading.
                imageView.setImageResource(android.R.color.transparent);


            }

            @Override
            protected String doInBackground(Void... params) {

                ImageProcessClass imageProcessClass = new ImageProcessClass();

                HashMap<String,String> HashMapParams = new HashMap<String,String>();

                HashMapParams.put(ImageName, GetImageNameEditText);

                HashMapParams.put(ImagePath, ConvertImage);

                // --


                String nama = inputNama.getText().toString();
                String alamat = inputAlamat.getText().toString();


                String FinalData = imageProcessClass.ImageHttpRequest(ServerUploadPath, HashMapParams);

                try {

                    if(id.isChecked()) {
                        bahasa = "Bahasa Indonesia,";
                    }

                    if(en.isChecked()) {
                        bahasa = bahasa + "Bahasa Inggris,";
                    }

                    if(kr.isChecked()) {
                        bahasa = bahasa + "Bahasa Korea,";
                    }

                    if(jp.isChecked()) {
                        bahasa = bahasa + "Bahasa Jepang,";
                    }

                    DefaultHttpClient client = new DefaultHttpClient();
                    String postURL = "http://178.128.83.56/crudxmlphp/tambahanggota.php";
                    HttpPost post = new HttpPost(postURL);

                    List<NameValuePair> param = new ArrayList<NameValuePair>();
                    param.add(new BasicNameValuePair("nama", nama));
                    param.add(new BasicNameValuePair("alamat", alamat));
                    param.add(new BasicNameValuePair("gender", jk));
                    param.add(new BasicNameValuePair("bahasa", bahasa));
                    param.add(new BasicNameValuePair("tanggal", tanggal));
                    param.add(new BasicNameValuePair("agama", agama));

                    UrlEncodedFormEntity ent = new UrlEncodedFormEntity(param,HTTP.UTF_8);
                    post.setEntity(ent);
                    HttpResponse responsePOST = client.execute(post);
                    HttpEntity resEntity = responsePOST.getEntity();
                    if (resEntity != null) {
                        Log.i("RESPONSE",EntityUtils.toString(resEntity));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(getApplicationContext(), SemuaAnggotaActivity.class);
                // tutup semua activity sebelumnya
                startActivity(i);
                finish();

                //--


                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();

        AsyncTaskUploadClassOBJ.execute();
    }





        private void showDateDialog()
        {

            /**
             * Calendar untuk mendapatkan tanggal sekarang
             */
            Calendar newCalendar = Calendar.getInstance();

            /**
             * Initiate DatePicker dialog
             */
            datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                    /**
                     * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                     */

                    /**
                     * Set Calendar untuk menampung tanggal yang dipilih
                     */
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);

                    /**
                     * Update TextView dengan tanggal yang kita pilih
                     */
                    tvDateResult.setText("Tanggal dipilih : "+dateFormatter.format(newDate.getTime()));
                    tanggal = dateFormatter.format(newDate.getTime());
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

            /**
             * Tampilkan DatePicker dialog
             */
            datePickerDialog.show();
        }

    public class ImageProcessClass{

        public String ImageHttpRequest(String requestURL,HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject ;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject ;
                BufferedReader bufferedReaderObject ;
                int RC ;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                httpURLConnectionObject.setReadTimeout(19000);

                httpURLConnectionObject.setConnectTimeout(19000);

                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null){

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {

                if (check)

                    check = false;
                else
                    stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        String firstItem = String.valueOf(spinner1.getSelectedItem());

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (firstItem.equals(String.valueOf(spinner1.getSelectedItem()))) {
                // ToDo when first item is selected
            } else {
                Toast.makeText(parent.getContext(),
                        "Kamu memilih : " + parent.getItemAtPosition(pos).toString(),
                        Toast.LENGTH_LONG).show();
                agama = parent.getItemAtPosition(pos).toString();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg) {

        }

    }




 
    /**
     * Background Async Task menambah anggota baru
     * */
    class BuatAnggotaBaru extends AsyncTask<String, String, String> {
     /**
     * Sebelum memulai background thread tampilkan Progress Dialog
    * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(TambahAnggotaActivity.this);
            pDialog.setMessage("Loading..Membuat Anggota Baru..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        
        /**
         * Menambah Data Anggota
         * */
        protected String doInBackground(String... args) {
        	
       	
        	String nama = inputNama.getText().toString();
            String alamat = inputAlamat.getText().toString();
           
            try {
                if(id.isChecked()) {
                    bahasa = "Bahasa Indonesia,";
                }

                if(en.isChecked()) {
                    bahasa = bahasa + "Bahasa Inggris,";
                }

                if(kr.isChecked()) {
                    bahasa = bahasa + "Bahasa Korea,";
                }

                if(jp.isChecked()) {
                    bahasa = bahasa + "Bahasa Jepang,";
                }

                DefaultHttpClient client = new DefaultHttpClient();  
                String postURL = "http://178.128.83.56/crudxmlphp/tambahanggota.php";
                HttpPost post = new HttpPost(postURL);

                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("nama", nama));
                    params.add(new BasicNameValuePair("alamat", alamat));
                    params.add(new BasicNameValuePair("gender", jk));
                    params.add(new BasicNameValuePair("bahasa", bahasa));
                    params.add(new BasicNameValuePair("tanggal", tanggal));
                    params.add(new BasicNameValuePair("agama", agama));

                    UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,HTTP.UTF_8);
                    post.setEntity(ent);
                    HttpResponse responsePOST = client.execute(post);  
                    HttpEntity resEntity = responsePOST.getEntity();  
                    if (resEntity != null) {    
                        Log.i("RESPONSE",EntityUtils.toString(resEntity));
                    }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent i = new Intent(getApplicationContext(), SemuaAnggotaActivity.class);
         // tutup semua activity sebelumnya
			startActivity(i);
			finish();
           return null;
        }


    /** 
     * setelah background task selesai hilangkan progress dialog
     * **/
    protected void onPostExecute(String file_url) {
        // dismiss the dialog once done
        pDialog.dismiss();
    }


   }


}

